# RosaeNLG and GraalJS

This sample project shows how to run [RosaeNLG](https://rosaenlg.org) on
top of [GraalJS](http://www.graalvm.org/docs/reference-manual/languages/js/).

The sample project used is the RosaeNLG tutorial.

Use [Graal Packager for RosaeNLG](https://gitlab.com/rosaenlg-projects/graal-packager) to package your RosaeNLG templates into a proper JS file.

In `js` folder, put:
- the packaged version of the RosaeNLG lib, here `rosaenlg_tiny_en_US_1.0.5.js`
- the compiled template, here `tutorial_en_US.js`
- a launcher js function that you write, here `fct.js`

Run `mvn package`, and then `mvn exec:exec`.

You should obtain texts about phones, like:
```
iteration: 156ms, res: <p>I really love the new OnePlus 5T. The phone's available tones are Black, Red and White. This phone has a display with a screen-to-body ratio of 80.43 % and a physical size of 6 inches along with a battery of 3,300 mAh.</p><p>The OnePlus 5 is really a fantastic phone. The phone's available tints are Gold and Gray. This phone has a battery of 3,300 mAh. It has a display with a physical size of 5.5 inches and a screen-to-body ratio of 72.93 %.</p><p>I really love the new OnePlus 3T. The phone's available tones are Black, Gold and Gray. This phone has a battery of 3,400 mAh. It has a display with a screen-to-body ratio of 73.15 % and a physical size of 5.5 inches.</p>
```


# Running GraalJS on stock JDK11

This is a simple maven project that demonstrates how it's possible to run
[GraalJS](http://www.graalvm.org/docs/reference-manual/languages/js/) on a
stock JDK11. The application is a simple JavaScript benchmark embedded in a
Java application which compares performance of GraalJS and Nashorn.

## Pre requirements

- Linux or Mac OS
- [Maven](https://maven.apache.org)
- [JDK11](https://jdk.java.net/11/)

## Setup

- Clone this repository
```
git clone https://github.com/graalvm/graal-js-jdk11-maven-demo
```

- Move to the newly cloned directory
```
cd graal-js-jdk11-maven-demo
```

- Make sure that JAVA_HOME is pointed at a JDK11
```
export JAVA_HOME=/path/to/jdk11
```

- Package the project using Maven
```
mvn package
```

## Execution

This project provides two execution setups (using the
[exec-maven-plugin](https://www.mojohaus.org/exec-maven-plugin/)). One uses the
Graal compiler to JIT compile JavaScript for better performance, and the other
does not and only interprets the JavaScript code. Both executions output
benchmark results for GraalJS (via the [GraalVM Polyglot
API](https://www.graalvm.org/truffle/javadoc/index.html?com/oracle/truffle/api/instrumentation/EventContext.html)
and the [Java Scripting
API](https://docs.oracle.com/javase/8/docs/technotes/guides/scripting/prog_guide/api.html)) and Nashorn.



To Execute with Graal run
```
mvn exec:exec
```

To Execute without Graal run
```
mvn exec:exec@nograal
```

The benchmark prints the time per iteration in milliseconds, so lower values are better.

## Running on GraalVM

This project is also setup to run on GraalVM. The setup is the same except
that your JAVA_HOME should point to a directory contain GraalVM. In this case,
execution without Graal is not supported.
