// written manually, call the template generation with RosaeNLG
function doit() {
  let rendered = templates_holder.tutorial_en_US({
    util: new rosaenlg_en_US.NlgLib({language: 'en_US'}),
  });
  return rendered;
}
